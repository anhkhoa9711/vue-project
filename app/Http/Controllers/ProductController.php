<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function getData(){
    	$product=Product::all();

    	return response($product, 200);
    }
}
