# Xử lý bất đồng bộ trong JavaScript
## Bất đồng bộ trong JS là gì:
Chạy thử 1 ví dụ sau trên console của browser hoặc chạy thử trên scrimba [Link](https://scrimba.com/c/c8PDrnsa):
```javascript
console.log("start")
setTimeout(function() {
	console.log("processing")
}, 500)
console.log("end")
```
- Kết quả đưa ra console không theo thứ tự start -> processing -> end mà là start -> end -> processing.
- Kết luận: Trong Javascript thì có một số trường hợp code sẽ không đợi dòng phía trên thực thi xong hẳn rồi mới thực thi dòng bên dưới. - Xử lý này được gọi là xử lý bất đồng bộ (Asynchronous) việc này làm cho javascript có tốc độ thực thi nhanh hơn các ngôn ngữ khác. 
- Hầu hết những xử lý cần có thời gian chờ như gọi ajax lên server, listen event, .. thì đều được thực thi theo kiểu này. Ở đây mình có một ví dụ về trường hợp gọi ajax [Link](https://scrimba.com/c/c8PDrJfK)

Vậy làm sao để đảm bảo console.log("end") được thực thi khi nào console.log("processing") thực hiện xong?
## Promise
Dựa trên tham khảo từ [Mozilla](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)<br>
Promise là một cách xử lý thường gặp để xử lý bất đồng bộ thường gặp trong JS. Ví dụ ở dưới sẽ đảm bảo được console sẽ hiển thị theo đúng thứ tự start -> processing -> end
```javascript
console.log("start")

new Promise(function(resolve, reject) {
  setTimeout(function() {
    resolve('processing');
  }, 300);
}).then(function () {
	console.log("end")
});
```
### Phân tích thành phần của 1 promise
1 promise gồm có 3 thành phần chính như ví dụ bên dưới:

 1. Bắt đầu promise: Ở vị trí này thì có 2 cái cần lưu ý  resolve và reject là 2 function, khi funtion resolve() được gọi thì báo hiệu kết thúc promise thành công và bắt đầu thực thi funtion trong **.then**, nếu funtion reject() được gọi thì báo hiệu kết thúc promise thất bại và bắt đầu thực thi fuction bên trong **.catch**.
 2. Kết thúc promise thành công: nằm bên trong  **.then**, được thực thi ngay sau khi funtion resolve() trong promise được gọi.
 3. kết thúc promise thất bại: nằm bên trong  **.catch**, được thực thi ngay sau khi funtion reject() trong promise được gọi.

```javascript
new Promise(function(resolve, reject) {
	//Bắt đầu promise
	if (success) {
		resolve(data)
	} else {
		resject(error)
	}
}).then(function () {
	//kết thúc promise thành công
	console.log("end")
}).catch(function () {
	//kết thúc promise thất bại
	console.log("error")
});
```
## async await
Dựa trên tham khảo từ [Mozilla](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function)
<br>
Đây là cách viết code đơn giản, dễ đọc nhất dùng để xử lý bất đồng bộ trong JS
<br>
Một async funtion sẽ được viết dưới dạng như sau:
```javascript
function wait2second () {
	return new Promise(function(resolve, reject) {
		setTimeout(function() {
		    resolve();
		}, 300);
	})
}

async function functionName() {
  console.log('start');
  // await dùng để chờ một promise thực thi xong. và chỉ được dùng trong async funtion
  var result = await wait2second();
  console.log('end');
}
```
Dưới đây là ví dụ về việc chờ lấy data từ server: [link](https://scrimba.com/c/cdJdVyfq)

## Tham khảo thêm
[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)<br>
[async await](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function)<br>

Tham khảo thêm những tính năng khác của ES6 để có thể làm project tốt hơn và đọc hiểu được code của người khác.<br>
[Dùng let - const thay cho var](http://es6-features.org/#BlockScopedVariables)<br>
[Spread operator](http://es6-features.org/#SpreadOperator)<br>
[String template](http://es6-features.org/#StringInterpolation)<br>
[Arrow function](http://es6-features.org/#ExpressionBodies)<br>
Có thể tìm hiểu tất cả các tính năng của ES6 tại trang [http://es6-features.org](http://es6-features.org/)<br>
