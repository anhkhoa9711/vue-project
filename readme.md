<p align="center"><img height=64 src="https://vuejs.org/images/logo.png"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## Cài đặt trước khi sử dụng

Chạy command sau trước khi sử dụng: 
```
composer install
npm install
```
## Template dùng cho project:
<a href="https://drive.google.com/open?id=17p3eUh1zbQ72vg6GY0Is_ahc8tlwzkrt">Download</a>

## Video:
 - <a target="_blank" href="https://drive.google.com/open?id=1Zm4A3l0228kmao5yovdXtB2_ZfC4HRlj" >Vue basic 1 (data - template)</a>
 - <a target="_blank" href="https://drive.google.com/open?id=13jLUS7Ll3IFx-MO6FtJV-JHVX-Q2VRoZ">Vue basic 2 (method - event - computed - filter)</a>