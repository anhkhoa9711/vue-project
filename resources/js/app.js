import _ from "lodash";
import Vue from "vue";
require("./bootstrap.js");
//let productList = require('./data/product');

// import component
import ShopFullWidth from "./view/ShopFullWidth";
import ShoppingCart from "./view/ShoppingCart";

new Vue({
    el: "#app",
    data: {
        cart: [],
        productList: [],
        currentPage: 'shop',
    },
    async beforeCreate(){
        let url = "http://127.0.0.1:8000/get-data"

        let result = await axios.get(url)
        //console.log(result);
        this.productList = result.data
    },
    components: {
        ShopFullWidth,
        ShoppingCart
    },
    methods: {
        addCart: function (item) {
            let product = _.clone(item);
            let itemInCart = _.find(this.cart, {id: product.id});
            if (itemInCart) {
                let index = this.cart.indexOf(itemInCart);
                this.cart[index].qty++;
            } else {
                product.qty = 1;
                this.cart.push(product)
            }
        },
        changePage: function (page) {
            this.currentPage = page
        },
        totalPrice : function(){
            let total = 0;
            this.cart.map(function(item){
                total = total+ (item.price *item.qty);
            });
            return total;
        },
        deleteItemCart : function(id){
            for(let i =0; i< this.cart.length;i++){
                if( (this.cart[i]).id == id){
                    this.cart.splice(i, 1);
                }
            }

        },
    }
    
});