# Cài ESLint cho Sublime Text 3
  - Giúp check lỗi cú pháp (thiếu dấu chấm, phẩy, ngoặc, ....)
  - Hỗ trợ code chuẩn hơn

## Các bước cài đặt
### 1. Cài Package cho Sublime Text
  - Mở sublime text -> preferrences -> package controll -> install package
  - Cài pakage SublimeLinter
  - Cài package SublimeLinter-eslint
### 2. Cài đặt setting cho package
  - Mở sublime text -> preferrences -> Package Settings -> SublimeLinter -> Settings
  - Copy nội dung sau paste vào file setting và save lại:
  ```javascript
  // SublimeLinter Settings - User
{
	"linters": {
	    "eslint": {
	        "selector": "text.html.vue, source.js - meta.attribute-with-value"
	    }
	}
}
```
### 3. Cài eslint cho máy
Chạy command sau:
```bash
npm install -g eslint
```
### 4. Tạo file config cho project
- Tạo file mới với tên .eslintrc.json ở thư mục /resources/js của project
- Copy nội dung sau vào file mới tạo và save lại:
```javascript
{
  "root": true,
  "env": {
    "node": true,
    "browser": true
  },
  "extends": [
    "plugin:vue/essential",
    "eslint:recommended"
  ],
  "parserOptions": {
    "parser": "babel-eslint"
  },
  "rules": {
    "no-console": "off"
  },
  "globals": {
    "_": "writable",
    "Popper": "writable",
    "$": "writable",
    "jQuery": "writable",
    "axios": "writable"
  }
}
```
### 5. Cài đặt các thư viện cần thiết cho project
Chạy command sau <b>tại thư mục gốc của project</b>:
```bash
npm install eslint eslint-plugin-vue babel-eslint --save-dev
```
## Lưu ý:
Bước 4 và 5 là tạo riêng cho từng project nên khi tạo project mới thì phải thực hiện lại bước 4 và 5

## Cách sử dụng
Khi save một file lại thì Sublime sẽ tự động validate file đó, những chỗ được khoanh đỏ là bị lỗi, hover lên chỗ đó để xem chi tiết về lỗi đó.
![Using ESlint](https://raw.githubusercontent.com/quangphuchuynh95/vue-project/master/document/image/eslint.png)



