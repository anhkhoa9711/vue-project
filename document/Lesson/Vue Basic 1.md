﻿# Lesson 1 - Vue Basic
- Các thành phần cơ bản khi tạo một ứng dụng Vuejs
## Các thành phần cơ bản của Vuejs
Khi Tạo một ứng dụng Vue cơ bản thì có 2 file chúng ta sẽ làm việc: 
1 file html/blade chứa html trả về cho người dùng
Cấu trúc cơ bản của html như sau
```html
<html>
	<head>
	</head>
	<body>
		<div  id="app"></div>
		
		<!-- Thẻ script dùng để đưa xử lý của file js bên dưới vào html -->
		<script  src="index.pack.js"></script>
	</body>
</html>
```
1 file js chứa xử lý bên dưới. có cấu trúc cơ bản như sau:
```javascript
// Vì sử dụng library của bên thứ 3 nên cần phải import vào
import Vue from  'vue';

// Khởi tạo ứng dụng Vue với syntax cơ bản như sau:
new Vue({
	el: '#app',  // String
	data: {},  // Object
	template:  '',  // String
});
```
Dựa vào file js thì có thể thấy những thành phần cơ bản của Vue như sau:
 - el: Là đối tượng được chọn để Vue tác động lên. Ở đây để là #app có nghĩa là sẽ tác động lên đối tượng có id="app" (sử dụng selector giống jQuery/CSS), bên ngoài đối tượng này sẽ không bị ảnh hưởng của Vue
 - data: Là nơi lưu trữ dữ liệu của Vue, giống như 1 database thu nhỏ của nó.
 - template: Quy định những gì sẽ hiển thị ra cho người dùng, khi Vue hoạt động thì html đối tượng được chọn ở **el** sẽ được thay thế bởi template
## Làm việc với template
### 1. Cách ghi template:
1. Ghi bằng thuộc tính template như ở trên (thuật ngữ trong Vue gọi là string template) [Xem code mẫu](https://scrimba.com/c/cyNQ2ZSe)
2. Ghi trực tiếp trong file html (Thuật ngữ của Vue gọi là in DOM template). Trường hợp không có template bên file js thì Vue sẽ tự động lấy nội dung html của đối tượng được chọn làm template.  [Xem code mẫu](https://scrimba.com/c/cZWQaaAz)

**Lưu ý do cách 2 dễ nhìn hơn nên code mẫu từ đây trở về sau sẽ dùng template ở cách thứ 2**
### 2. Đưa data vào template:
Có 2 cách:
 - Sử dụng dấu {{ }} tương tự như laravel blade ( cách này chỉ áp dụng khi đưa data trực tiếp vào html)  [Xem code mẫu](https://scrimba.com/c/cZWQagC6)
  - Sử dụng v-bind (cách này cần thiết khi đưa data vào thuộc tính của thẻ html) [Xem code mẫu](https://scrimba.com/c/ckMBwaH9)

### 3. Dùng vòng lặp để hiển thị Array/Object ra template
- Mục đích: Để hiển thị một danh sách ví dụ như danh sách sản phẩm, danh sách khách hàng, ...
- Để hiển thị danh sách thì trong Vue sẽ sử dụng cấu trúc v-for như sau: [Xem code mẫu](https://scrimba.com/c/cGv966Uy)
- Hãy đến [link này](https://scrimba.com/c/crNDvQtv) và thử tạo cho mình 1 danh sách tùy thích.

### 4. Dùng if/else để hiển thị ra template với điều kiện nhất định

Để hiển thị có điều kiện 1 phần tử thì trong Vue sẽ sử dụng cấu trúc v-if v-if-else v-else như sau: [Code mẫu](https://scrimba.com/c/cndgvKud)

